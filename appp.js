
var myApp = angular.module('myApp', []);
myApp.controller('myController', function ($scope) {


    $scope.products = [
        {name: 'Book 1', price : 200},
        {name: 'Book 2', price : 200},
        {name: 'Book 3',price : 200 }
    ];
    $scope.cart = [];
    $scope.total = 0;
    $scope.addToCart = function (item) {
        item.qty = 1;
        $scope.total = $scope.total + item.price;
        $scope.cart.push(item)
    }

});